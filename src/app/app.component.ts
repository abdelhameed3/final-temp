import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { KTUtil } from '../assets/js/components/util';
import KTLayoutAsideToggle from '../assets/js/layout/base/aside-toggle';
import KTLayoutStickyCard from '../assets/js/layout/base/sticky-card';
import KTLayoutStretchedCard from '../assets/js/layout/base/stretched-card';
import KTLayoutBrand from '../assets/js/layout/base/brand';
import KTLayoutAside from '../assets/js/layout/base/aside';
import KTLayoutAsideMenu from '../assets/js/layout/base/aside-menu';
import { LayoutService } from './modules/shared/services/layout.service';
import { LayoutInitService } from './modules/shared/services/layout-init.service';
import { LayoutComponent } from './modules/shared/components/_layout/layout.component';
import { LanguageHandlerService } from './modules/shared/services/language-handler.service';
import { SplashScreenService } from './modules/shared/components/_layout/splash-screen/splash-screen.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends LayoutComponent implements OnInit, OnDestroy {
  asideSelfMinimizeToggle = false;
  isAsideShown = true;
  contentContainerClasses = '';
  contentClasses = '';
  headerCSSClasses: string;
  asideCSSClasses: string;
  headerMobileClasses = '';

  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  constructor(
    private router: Router,
    private layoutService: LayoutService,
    private initService: LayoutInitService,
    private splashScreenService: SplashScreenService,
    private languageHandler: LanguageHandlerService
  ) {
    super(layoutService);
    this.initService.init();
  }

  ngOnInit() {
    const currentLang = localStorage.getItem('user-language');

    if (currentLang == null) {
      localStorage.setItem('user-language', 'ar');
      // this.translate.use('ar');
      this.langChanged('ar');
    } else {
      this.langChanged(currentLang);
    }

    this.languageHandler.setAppLanguageAndDirectionOnLoad();

    this.isAsideShown = this.layoutService.getProp('aside.self.display');
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
        setTimeout(() => {
          document.body.classList.add('page-loaded');
        }, 500);
      }
    });

    this.asideSelfMinimizeToggle = this.layoutService.getProp(
      'aside.self.minimize.toggle'
    );

    this.contentContainerClasses = this.layoutService.getStringCSSClasses(
      'content_container'
    );
    this.contentClasses = this.layoutService.getStringCSSClasses('content');
    this.headerCSSClasses = this.layoutService.getStringCSSClasses('header');
    this.asideCSSClasses = this.layoutService.getStringCSSClasses('aside');

    this.headerMobileClasses = this.layoutService.getStringCSSClasses(
      'header_mobile'
    );
    this.hideLayoutInAuthModule();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
        this.hideLayoutInAuthModule();
        setTimeout(() => {
          document.body.classList.add('page-loaded');
        }, 500);
      }
    });
  }

  hideLayoutInAuthModule() {
    const pageUrl = window.location.href;

    if (pageUrl.includes('/auth')) {
      this.selfLayout = 'blank';
    } else {
      this.selfLayout = 'default';
    }
  }

  ngAfterViewInit() {
    KTUtil.ready(() => {
      // Init Brand Panel For Logo
      KTLayoutBrand.init('kt_brand');
      // Init Aside
      KTLayoutAside.init('kt_aside');
      // Init Aside Menu
      KTLayoutAsideMenu.init('kt_aside_menu');

      if (this.asideSelfMinimizeToggle) {
        // Init Aside Menu Toggle
        KTLayoutAsideToggle.init('kt_aside_toggle');
      }

      // Init Sticky Card
      KTLayoutStickyCard.init('kt_page_sticky_card');
      // Init Stretched Card
      KTLayoutStretchedCard.init('kt_page_stretched_card');
    });
  }
  langChanged(lang) {
    const elEn = document.querySelector('#angular-en');
    const elAr = document.querySelector('#angular-ar');

    if (lang === 'ar') {
      // add bootstrap ar
      if (elEn) {
        elEn.remove();
      }
      this.generateLinkElement({
        id: 'angular-ar',
        href: './assets/sass/style.angular.rtl.css',
        dir: 'rtl',
        lang: 'ar',
      }).then((res: any) => {
        if (res.success) {
          console.log('Done Adding Element to Dom');
          const routerSubscription = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
              setTimeout(() => {
                // hide splash screen
                this.splashScreenService.hide();

                // scroll to top on every route change
                window.scrollTo(0, 0);

                // to display back the body content
                document.body.classList.add('page-loaded');
              }, 500);
            }
          });
          this.unsubscribe.push(routerSubscription);
        }
      });
    } else {
      // en
      if (elAr) {
        elAr.remove();
      }
      this.generateLinkElement({
        id: 'angular-en',
        href: './assets/sass/style.angular.css',
        dir: 'ltr',
        lang: 'en',
      }).then((res: any) => {
        if (res.success) {
          console.log('Done Adding Element to Dom');
          const routerSubscription = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
              // to display back the body content
              setTimeout(() => {
                // hide splash screen
                this.splashScreenService.hide();

                // scroll to top on every route change
                window.scrollTo(0, 0);

                document.body.classList.add('page-loaded');
              }, 500);
            }
          });
          this.unsubscribe.push(routerSubscription);
        }
      });
    }
  }
  async generateLinkElement(props) {
    return new Promise((resolve, reject) => {
      const el = document.createElement('link');
      const htmlEl = document.getElementsByTagName('html')[0];
      el.rel = 'stylesheet';
      el.href = props.href;
      el.id = props.id;
      document.head.append(el);
      htmlEl.setAttribute('dir', props.dir);
      htmlEl.setAttribute('lang', props.lang);
      console.log('Done');

      resolve({ success: true });
    });
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}
