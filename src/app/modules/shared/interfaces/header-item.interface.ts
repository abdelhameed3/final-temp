export interface HeaderItem {
  title: string;
  root: boolean;
  alignment: string;
  page: string;
  translate: string;
}
