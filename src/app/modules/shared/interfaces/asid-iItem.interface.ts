export interface AsideItem {
  title: string;
  root: boolean;
  icon: string;
  svg: string;
  page: string;
  bullet: string;
}
