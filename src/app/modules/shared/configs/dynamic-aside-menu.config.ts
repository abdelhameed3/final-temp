export const DynamicAsideMenuConfig = {
  items: [
    {
      title: 'dashboard',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/icons/Design/Layers.svg',
      page: '/dashboard',
      bullet: 'dot',
    },
    {
      title: 'pray',
      root: true,
      bullet: 'dot',
      page: '/islamic',
      icon: 'flaticon2-digital-marketing',
      svg: './assets/media/svg/icons/Design/Layers.svg',
      submenu: [
        {
          title: 'list',
          page: '/islamic/list'
        },
        {
          title: 'edit',
          page: '/islamic/edit'
        },
      ]
    },
    // { section: 'Applications' },
  ],
};
