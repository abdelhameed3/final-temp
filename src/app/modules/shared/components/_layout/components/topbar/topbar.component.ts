import { Component, OnInit, AfterViewInit } from '@angular/core';
import KTLayoutQuickUser from '../../../../../../../assets/js/layout/extended/quick-user';
import KTLayoutHeaderTopbar from '../../../../../../../assets/js/layout/base/header-topbar';
import { KTUtil } from '../../../../../../../assets/js/components/util';
import { LayoutService } from '@app/modules/shared/services/layout.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent implements OnInit, AfterViewInit {
  extrasUserDisplay: boolean;
  extrasLanguagesDisplay: boolean;
  extrasUserLayout: 'offcanvas' | 'dropdown';

  constructor(private layout: LayoutService) {}

  ngOnInit(): void {
    // topbar extras
    this.extrasUserDisplay = this.layout.getProp('extras.user.display');
    this.extrasUserLayout = this.layout.getProp('extras.user.layout');
    this.extrasLanguagesDisplay = this.layout.getProp(
      'extras.languages.display'
    );
  }

  ngAfterViewInit(): void {
    KTUtil.ready(() => {
      if (this.extrasUserDisplay && this.extrasUserLayout === 'offcanvas') {
        KTLayoutQuickUser.init('kt_quick_user');
      }
      KTLayoutHeaderTopbar.init('kt_header_mobile_topbar_toggle');
    });
  }
}
