import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

interface LanguageFlag {
  lang: string;
  name: string;
  flag: string;
  active?: boolean;
}

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent implements OnInit {
  language: LanguageFlag;
  languages: LanguageFlag[] = [
    {
      lang: 'ar',
      name: 'Arabic',
      flag: './assets/media/svg/flags/158-egypt.svg',
    },
    {
      lang: 'en',
      name: 'English',
      flag: './assets/media/svg/flags/226-united-states.svg',
    },
  ];
  constructor(private router: Router) {}

  ngOnInit() {
    this.setLanguage(localStorage.getItem('user-language'));
  }

  setLanguageWithRefresh(lang) {
    this.setLanguage(lang);
    console.log(lang);
    localStorage.setItem('user-language', lang);
    window.location.reload();
  }

  setLanguage(lang) {
    this.languages.forEach((language: LanguageFlag) => {
      if (language.lang === lang) {
        language.active = true;
        this.language = language;
      } else {
        language.active = false;
      }
    });
  }
}
