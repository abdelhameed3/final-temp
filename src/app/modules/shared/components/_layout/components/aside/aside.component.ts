import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LayoutService } from '@app/modules/shared/services/layout.service';
import { DynamicAsideMenuConfig } from '@app/modules/shared/configs/dynamic-aside-menu.config';
import { AsideItem } from '@app/modules/shared/interfaces/asid-iItem.interface';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
})
export class AsideComponent implements OnInit {
  disableAsideSelfDisplay: boolean;
  headerLogo: string;
  brandSkin: string;
  ulCSSClasses: string;
  location: Location;
  asideMenuHTMLAttributes: any = {};
  asideMenuCSSClasses: string;
  asideMenuDropdown;
  brandClasses: string;
  asideMenuScroll = 1;
  asideSelfMinimizeToggle = false;
  environment = environment;
  errorMessage: string;
  isUserAdmin: boolean;
  isPageSingleAppPage: boolean;
  applicationsList: any;
  userData: any;
  appID: any;
  activeApp: any;
  asideItems: AsideItem[];

  constructor(private layout: LayoutService, private loc: Location) {}

  ngOnInit(): void {
    this.asideItems = DynamicAsideMenuConfig.items;
    this.disableAsideSelfDisplay =
      this.layout.getProp('aside.self.display') === false;
    this.brandSkin = this.layout.getProp('brand.self.theme');
    this.headerLogo = this.getLogo();
    this.ulCSSClasses = this.layout.getProp('aside_menu_nav');
    this.asideMenuCSSClasses = this.layout.getStringCSSClasses('aside_menu');
    this.asideMenuHTMLAttributes = this.layout.getHTMLAttributes('aside_menu');
    this.asideMenuDropdown = this.layout.getProp('aside.menu.dropdown')
      ? '1'
      : '0';
    this.brandClasses = this.layout.getProp('brand');
    this.asideSelfMinimizeToggle = this.layout.getProp(
      'aside.self.minimize.toggle'
    );
    this.asideMenuScroll = this.layout.getProp('aside.menu.scroll') ? 1 : 0;
    this.asideMenuCSSClasses = `${this.asideMenuCSSClasses} ${
      this.asideMenuScroll === 1 ? 'scroll my-4 ps ps--active-y' : ''
    }`;
    // Routing
    this.location = this.loc;
  }

  private getLogo() {
    return './assets/media/logos/logo.png';
  }
}
