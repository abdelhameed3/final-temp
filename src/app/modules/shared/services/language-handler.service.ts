import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class LanguageHandlerService {
  private LANGUAGE_KEY = 'user-language';
  constructor(
    @Inject(DOCUMENT) private doc: Document,
    public translator: TranslateService
  ) {
    this.changeAppDirection = this.changeAppDirection.bind(this);
    this.changeTranslationLanguage = this.changeTranslationLanguage.bind(this);
    this.updateStorageWithLanguageKey = this.updateStorageWithLanguageKey.bind(
      this
    );
  }

  getLanguageKey(): string | JSON | null {
    return localStorage.getItem(this.LANGUAGE_KEY);
  }

  changeAppDirection(dir: string): void {
    this.doc.dir = dir;
  }

  changeTranslationLanguage(langKey: string): void {
    this.translator.use(langKey);
  }

  updateStorageWithLanguageKey(langKey: string): void {
    localStorage.setItem(this.LANGUAGE_KEY, langKey);
  }

  setAppLanguageAndDirectionOnLoad(): void {
    const languageKey = this.getLanguageKey();

    if (languageKey) {
      if (languageKey === 'en') {
        this.changeAppDirection('ltr');
        this.changeTranslationLanguage('en');
      } else {
        this.changeAppDirection('rtl');
        this.changeTranslationLanguage('ar');
      }
    } else {
      this.updateStorageWithLanguageKey('ar');
      this.changeAppDirection('rtl');
      this.changeTranslationLanguage('ar');
    }
  }
}
