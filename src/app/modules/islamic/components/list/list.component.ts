import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(
    private modal: NgbModal,
  ) { }

  ngOnInit(): void {
  }

  open(content) {
    this.modal.open(content);
  }

}
