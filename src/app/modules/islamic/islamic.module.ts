import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditComponent } from './components/edit/edit.component';
import { ListComponent } from './components/list/list.component';
import { IslamicRoutingModule } from './islamic-routing.module';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [EditComponent, ListComponent],
  imports: [
    CommonModule,
    IslamicRoutingModule,
    FormsModule,
    SharedModule,
    NgbTooltipModule,
    ReactiveFormsModule,
    TranslateModule,
    MatProgressSpinnerModule
  ]
})
export class IslamicModule { }
