import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExampleComponent } from './example.component';
import { ExampleOneComponent } from './components/example-one/example-one.component';

const routes: Routes = [
  { path: '', component: ExampleComponent },
  { path: 'example', component: ExampleOneComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleRoutingModule {}
