import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExampleRoutingModule } from './example-routing.module';
import { ExampleComponent } from './example.component';
import { ExampleOneComponent } from './components/example-one/example-one.component';

@NgModule({
  declarations: [ExampleComponent, ExampleOneComponent],
  imports: [CommonModule, ExampleRoutingModule],
})
export class ExampleModule {}
