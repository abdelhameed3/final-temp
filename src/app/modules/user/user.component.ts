import { Component, OnInit } from '@angular/core';

type CarouselImage = {
  bgColor: string;
  path: string;
};

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  currentYear: string;

  today: Date = new Date();

  constructor() {}

  ngOnInit(): void {
    const currentDate = new Date();
    this.currentYear = currentDate.getFullYear().toString();
  }
}
