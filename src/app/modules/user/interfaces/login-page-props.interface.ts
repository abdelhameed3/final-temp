export interface LoginPageProps {
  isLogging: boolean;
  errorMessage: string;
}
