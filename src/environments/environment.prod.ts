export const environment = {
  production: true,
  appVersion: 'sisappenza2020',
  baseApiUrl: 'https://localhost:44365/api',
};
